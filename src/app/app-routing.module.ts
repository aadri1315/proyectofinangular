import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './components/inicio/inicio.component';
import { CreateLibrosComponent } from './components/libros/create-libros/create-libros.component';
import { ListLibrosComponent } from './components/libros/list-libros/list-libros.component';
import { ErrorComponent } from './components/error/error.component';
import { EditLibrosComponent } from './components/libros/edit-libros/edit-libros.component';
import { ListSociosComponent } from './components/socios/list-socios/list-socios.component';
import { CreateSociosComponent } from './components/socios/create-socios/create-socios.component';
import { EditSociosComponent } from './components/socios/edit-socios/edit-socios.component';
import { ListPrestamosComponent } from './components/prestamos/list-prestamos/list-prestamos.component';
import { CreatePrestamosComponent } from './components/prestamos/create-prestamos/create-prestamos.component';

const routes: Routes = [
  { path: '', redirectTo: 'inicio', pathMatch: 'full' },
  { path: 'inicio', component: InicioComponent },
  { path: 'list-libros', component: ListLibrosComponent },
  { path: 'create-libros', component: CreateLibrosComponent },
  { path: 'edit-libros/:id', component: EditLibrosComponent },
  { path: 'list-socios', component: ListSociosComponent },
  { path: 'create-socios', component: CreateSociosComponent },
  { path: 'edit-socios/:id', component: EditSociosComponent },
  { path: 'list-prestamos', component: ListPrestamosComponent },
  { path: 'create-prestamos', component: CreatePrestamosComponent },
  { path: 'error', component: ErrorComponent },
  { path: '**', redirectTo: 'error', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
