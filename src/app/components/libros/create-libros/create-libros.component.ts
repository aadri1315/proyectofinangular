import { Component, OnInit, NgModule } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { LibroService } from '../../../services/libro.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-create-libros',
  templateUrl: './create-libros.component.html',
  styleUrls: ['./create-libros.component.css'],
})
export class CreateLibrosComponent implements OnInit {
  er = new FormControl('', [Validators.required]);
  createLibro: FormGroup;
  loading = false;

  constructor(
    private fb: FormBuilder,
    private _libroService: LibroService,
    private router: Router,
    private _snackBar: MatSnackBar
  ) {
    this.createLibro = this.fb.group({
      title: ['', Validators.required],
      genre: ['', Validators.required],
      author: ['', Validators.required],
      price: ['', Validators.required],
      infor: ['', Validators.required],
      front: ['', Validators.required],
    });
  }

  ngOnInit(): void {}

  getErrorMessage() {
    if (this.er.hasError('required')) {
      return 'You must enter a value';
    }

    return '';
  }

  addLibro() {
    if (this.createLibro.invalid) {
      return;
    }

    const libro: any = {
      title: this.createLibro.value.title,
      genre: this.createLibro.value.genre,
      author: this.createLibro.value.author,
      infor: this.createLibro.value.infor,
      price: this.createLibro.value.price,
      front: this.createLibro.value.front,
      prestado: false,
      fechaCreado: new Date(),
      fechaEditado: new Date(),
    };

    this.loading = true;
    this._libroService
      .addLibro(libro)
      .then(() => {
        this._snackBar.open('Libro guardado.', 'Bien', {
          duration: 1500,
          horizontalPosition: 'end',
        });
        this.loading = false;
        this.router.navigate(['/list-libros']);
      })
      .catch((err: any) => {
        console.log(err);
        this.loading = false;
      });
  }
}
