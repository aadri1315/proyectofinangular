import { LibroService } from './../../../services/libro.service';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PrestamoService } from 'src/app/services/prestamo.service';

@Component({
  selector: 'app-list-libros',
  templateUrl: './list-libros.component.html',
  styleUrls: ['./list-libros.component.css'],
})
export class ListLibrosComponent implements OnInit {
  libros: any[] = [];

  constructor(
    private _libroService: LibroService,
    private _snackBar: MatSnackBar,
    private _prestamoService: PrestamoService
  ) {}

  ngOnInit(): void {
    this.getLibros();
  }

  getLibros() {
    this._libroService.getLibros().subscribe((data) => {
      this.libros = [];
      data.forEach((element: any) => {
        this.libros.push({
          id: element.payload.doc.id,
          ...element.payload.doc.data(),
        });
      });
    });
  }

  deleteLibro(id: string) {
    this._libroService
      .deleteLibro(id)
      .then(() => {
        this._snackBar.open('Libro borrado.', 'Bien', {
          duration: 1500,
          horizontalPosition: 'end',
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
}
