import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LibroService } from '../../../services/libro.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-edit-libros',
  templateUrl: './edit-libros.component.html',
  styleUrls: ['./edit-libros.component.css'],
})
export class EditLibrosComponent implements OnInit {
  er = new FormControl('', [Validators.required]);
  editLibro: FormGroup;
  submitted = false;
  loading = false;
  id: string | null;

  constructor(
    private fb: FormBuilder,
    private _libroService: LibroService,
    private router: Router,
    private _snackBar: MatSnackBar,
    private ruta: ActivatedRoute
  ) {
    this.editLibro = this.fb.group({
      title: ['', Validators.required],
      genre: ['', Validators.required],
      author: ['', Validators.required],
      price: ['', Validators.required],
    });
    this.id = this.ruta.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    if (this.id !== null) {
      this.editarLibro();
    }
  }

  getErrorMessage() {
    if (this.er.hasError('required')) {
      return 'You must enter a value';
    }

    return '';
  }

  editarLibro() {
    if (this.id !== null) {
      this.loading = true;
      this._libroService.getLibro(this.id).subscribe((data) => {
        this.loading = false;
        this.editLibro.setValue({
          title: data.payload.data()['title'],
          genre: data.payload.data()['genre'],
          author: data.payload.data()['author'],
          price: data.payload.data()['price'],
        });
      });
    }
  }

  editando() {
    const libro: any = {
      title: this.editLibro.value.title,
      genre: this.editLibro.value.genre,
      author: this.editLibro.value.author,
      price: this.editLibro.value.price,
      fechaEditado: new Date(),
    };

    this.loading = true;

    if (this.id !== null) {
      this._libroService
        .actualizarLibro(this.id, libro)
        .then(() => {
          this.loading = false;
          this._snackBar.open('Libro actualizado.', 'Bien', {
            duration: 1500,
            horizontalPosition: 'end',
          });
          this.loading = false;
          this.router.navigate(['/list-libros']);
        })
        .catch((err: any) => {
          console.log(err);
          this.loading = false;
        });

      this.router.navigate(['/list-libros']);
    }
  }
}
