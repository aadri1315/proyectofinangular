import { SocioService } from './../../../services/socio.service';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-list-socios',
  templateUrl: './list-socios.component.html',
  styleUrls: ['./list-socios.component.css'],
})
export class ListSociosComponent implements OnInit {
  socios: any[] = [];

  constructor(
    private _socioService: SocioService,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.getSocios();
  }

  getSocios() {
    this._socioService.getSocios().subscribe((data) => {
      this.socios = [];
      data.forEach((element: any) => {
        this.socios.push({
          id: element.payload.doc.id,
          ...element.payload.doc.data(),
        });
      });
    });
  }

  deleteSocio(id: string) {
    this._socioService
      .deleteSocio(id)
      .then(() => {
        this._snackBar.open('Socio borrado.', 'Bien', {
          duration: 1500,
          horizontalPosition: 'end',
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
}
