import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSociosComponent } from './edit-socios.component';

describe('EditSociosComponent', () => {
  let component: EditSociosComponent;
  let fixture: ComponentFixture<EditSociosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditSociosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSociosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
