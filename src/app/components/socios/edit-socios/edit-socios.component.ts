import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SocioService } from '../../../services/socio.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-edit-socios',
  templateUrl: './edit-socios.component.html',
  styleUrls: ['./edit-socios.component.css'],
})
export class EditSociosComponent implements OnInit {
  er = new FormControl('', [Validators.required]);
  editSocio: FormGroup;
  submitted = false;
  loading = false;
  id: string | null;

  constructor(
    private fb: FormBuilder,
    private _socioService: SocioService,
    private router: Router,
    private _snackBar: MatSnackBar,
    private ruta: ActivatedRoute
  ) {
    this.editSocio = this.fb.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      age: ['', Validators.required],
      email: ['', Validators.required],
    });
    this.id = this.ruta.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    if (this.id !== null) {
      this.editarSocio(this.id);
    }
  }

  getErrorMessage() {
    if (this.er.hasError('required')) {
      return 'You must enter a value';
    }

    return '';
  }

  editarSocio(id: string) {
    if (this.id !== null) {
      this.loading = true;
      this._socioService.getSocio(this.id).subscribe((data) => {
        this.loading = false;
        this.editSocio.setValue({
          name: data.payload.data()['name'],
          surname: data.payload.data()['surname'],
          age: data.payload.data()['age'],
          email: data.payload.data()['email'],
        });
      });
    }
  }

  editando() {
    const socio: any = {
      name: this.editSocio.value.name,
      surname: this.editSocio.value.surname,
      age: this.editSocio.value.age,
      email: this.editSocio.value.email,
      fechaEditado: new Date(),
    };

    this.loading = true;

    if (this.id !== null) {
      this._socioService
        .actualizarSocio(this.id, socio)
        .then(() => {
          this.loading = false;
          this._snackBar.open('Socio actualizado.', 'Bien', {
            duration: 1500,
            horizontalPosition: 'end',
          });
          this.loading = false;
          this.router.navigate(['/list-socios']);
        })
        .catch((err: any) => {
          console.log(err);
          this.loading = false;
        });

      this.router.navigate(['/list-socios']);
    }
  }
}
