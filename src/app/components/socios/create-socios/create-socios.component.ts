import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { SocioService } from '../../../services/socio.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-create-socios',
  templateUrl: './create-socios.component.html',
  styleUrls: ['./create-socios.component.css'],
})
export class CreateSociosComponent implements OnInit {
  er = new FormControl('', [Validators.required]);
  createSocio: FormGroup;
  loading = false;

  //test
  constructor(
    private fb: FormBuilder,
    private _socioService: SocioService,
    private router: Router,
    private _snackBar: MatSnackBar
  ) {
    this.createSocio = this.fb.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      age: ['', Validators.required],
      email: ['', Validators.required],
    });
  }

  ngOnInit(): void {}

  getErrorMessage() {
    if (this.er.hasError('required')) {
      return 'You must enter a value';
    }

    return '';
  }

  addSocio() {
    if (this.createSocio.invalid) {
      return;
    }

    const socio: any = {
      name: this.createSocio.value.name,
      surname: this.createSocio.value.surname,
      age: this.createSocio.value.age,
      email: this.createSocio.value.email,
      fechaCreado: new Date(),
      fechaEditado: new Date(),
    };

    this.loading = true;
    this._socioService
      .addSocio(socio)
      .then(() => {
        this._snackBar.open('Socio guardado.', 'Bien', {
          duration: 1500,
          horizontalPosition: 'end',
        });
        this.loading = false;
        this.router.navigate(['/list-socios']);
      })
      .catch((err: any) => {
        console.log(err);
        this.loading = false;
      });
  }
}
