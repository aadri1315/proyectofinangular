import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSociosComponent } from './create-socios.component';

describe('CreateSociosComponent', () => {
  let component: CreateSociosComponent;
  let fixture: ComponentFixture<CreateSociosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateSociosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSociosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
