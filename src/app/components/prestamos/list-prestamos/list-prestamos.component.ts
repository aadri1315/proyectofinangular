import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PrestamoService } from 'src/app/services/prestamo.service';
import { LibroService } from 'src/app/services/libro.service';

@Component({
  selector: 'app-list-prestamos',
  templateUrl: './list-prestamos.component.html',
  styleUrls: ['./list-prestamos.component.css'],
})
export class ListPrestamosComponent implements OnInit {
  prestamos: any[] = [];

  constructor(
    private _prestamoService: PrestamoService,
    private _snackBar: MatSnackBar,
    private _libroService: LibroService
  ) {}

  ngOnInit(): void {
    this.getPrestamos();
  }

  getPrestamos() {
    this._prestamoService.getPrestamos().subscribe((data) => {
      this.prestamos = [];
      data.forEach((element: any) => {
        this.prestamos.push({
          id: element.payload.doc.id,
          ...element.payload.doc.data(),
        });
      });
    });
  }

  actualizarPrestamo(id: string, libroId: string) {
    this._prestamoService.deletePrestamo(id);

    this._libroService.actualizarPrestado(libroId, false);
  }
}
