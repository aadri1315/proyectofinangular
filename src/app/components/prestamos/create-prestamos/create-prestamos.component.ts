import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { PrestamoService } from 'src/app/services/prestamo.service';
import { LibroService } from 'src/app/services/libro.service';
import { SocioService } from 'src/app/services/socio.service';

@Component({
  selector: 'app-create-prestamos',
  templateUrl: './create-prestamos.component.html',
  styleUrls: ['./create-prestamos.component.css'],
})
export class CreatePrestamosComponent implements OnInit {
  createPrestamo: FormGroup;
  er = new FormControl('', [Validators.required]);
  nameid = new FormControl('', Validators.required);
  tidtulo = new FormControl('', Validators.required);
  titles: any[] = [];
  b: any[] = [];
  names: any[] = [];

  constructor(
    private fb: FormBuilder,
    private _prestamoService: PrestamoService,
    private _snackBar: MatSnackBar,
    private router: Router,
    private _libroService: LibroService,
    private _socioService: SocioService
  ) {
    this.createPrestamo = this.fb.group({
      title: this.tidtulo,
      name: this.nameid,
    });
  }

  ngOnInit(): void {
    this.getTitles();
    this.getNames();
  }

  getErrorMessage() {
    if (this.er.hasError('required')) {
      return 'You must enter a value';
    }

    return '';
  }

  getTitles() {
    this._libroService.getLibros().subscribe((data) => {
      let i = 0;
      let t = 0;
      data.forEach((element: any) => {
        this.titles.push({
          id: element.payload.doc.id,
          ...element.payload.doc.data(),
        });

        if (this.titles[i].prestado) {
          this.b.push(i - t);
          t++;
        }
        i++;
      });
      for (let f = 0; f < this.b.length; f++) {
        this.titles.splice(this.b[f], 1);
      }
      return;
    });
  }

  getNames() {
    this._socioService.getSocios().subscribe((data) => {
      this.names = [];
      data.forEach((element: any) => {
        this.names.push({
          id: element.payload.doc.id,
          ...element.payload.doc.data(),
        });
      });
    });
  }

  addPrestamo() {
    if (this.createPrestamo.invalid) {
      return;
    }

    this._libroService.actualizarPrestado(
      this.createPrestamo.value.title[1],
      true
    );

    const prestamo: any = {
      title: this.createPrestamo.value.title[0],
      name: this.createPrestamo.value.name,
      prestado: true,
      libroId: this.createPrestamo.value.title[1],
      fechaPrestamo: new Date(),
    };

    this._prestamoService
      .addPrestamo(prestamo)
      .then(() => {
        this._snackBar.open('Libro prestado.', 'Bien', {
          duration: 1500,
          horizontalPosition: 'end',
        });
        this.router.navigate(['/list-prestamos']);
      })
      .catch((err: any) => {
        console.log(err);
      });
  }
}
