import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PrestamoService {
  titles: any[] = [];

  constructor(private firestore: AngularFirestore) {}

  addPrestamo(prestamo: any): Promise<any> {
    return this.firestore.collection('prestamos').add(prestamo);
  }

  getPrestamos(): Observable<any> {
    return this.firestore.collection('prestamos').snapshotChanges();
  }

  actualizarPrestamos(id: string, prestamo: any): Promise<any> {
    return this.firestore.collection('prestamos').doc(id).update(prestamo);
  }

  getNames(): Observable<any> {
    return this.firestore.collection('socios').snapshotChanges();
  }

  deletePrestamo(id: string): Promise<any> {
    return this.firestore.collection('prestamos').doc(id).delete();
  }
}
