import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SocioService {
  constructor(private firestore: AngularFirestore) {}

  addSocio(socio: any): Promise<any> {
    return this.firestore.collection('socios').add(socio);
  }

  getSocios(): Observable<any> {
    return this.firestore.collection('socios').snapshotChanges();
  }

  deleteSocio(id: string): Promise<any> {
    return this.firestore.collection('socios').doc(id).delete();
  }

  getSocio(id: string): Observable<any> {
    return this.firestore.collection('socios').doc(id).snapshotChanges();
  }

  actualizarSocio(id: string, socio: any): Promise<any> {
    return this.firestore.collection('socios').doc(id).update(socio);
  }
}
