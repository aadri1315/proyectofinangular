import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListLibrosComponent } from './components/libros/list-libros/list-libros.component';
import { CreateLibrosComponent } from './components/libros/create-libros/create-libros.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { MatButtonModule } from '@angular/material/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { ErrorComponent } from './components/error/error.component';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { AngularFireModule } from '@angular/fire/compat';
import { environment } from 'src/environments/environment';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { EditLibrosComponent } from './components/libros/edit-libros/edit-libros.component';
import { CreateSociosComponent } from './components/socios/create-socios/create-socios.component';
import { EditSociosComponent } from './components/socios/edit-socios/edit-socios.component';
import { ListSociosComponent } from './components/socios/list-socios/list-socios.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { CreatePrestamosComponent } from './components/prestamos/create-prestamos/create-prestamos.component';
import { ListPrestamosComponent } from './components/prestamos/list-prestamos/list-prestamos.component';
import { MatSelectModule } from '@angular/material/select';
import { InicioComponent } from './components/inicio/inicio.component';
import { MatGridListModule } from '@angular/material/grid-list';

@NgModule({
  declarations: [
    AppComponent,
    ListLibrosComponent,
    CreateLibrosComponent,
    NavbarComponent,
    ErrorComponent,
    EditLibrosComponent,
    CreateSociosComponent,
    EditSociosComponent,
    ListSociosComponent,
    CreatePrestamosComponent,
    ListPrestamosComponent,
    InicioComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatButtonModule,
    MatIconModule,
    MatGridListModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    BrowserAnimationsModule,
    MatPaginatorModule,
    MatSelectModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
